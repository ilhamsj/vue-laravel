<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        // return new CategoryCollection(Category::with('products')->orderBy('updated_at', 'desc')->get());
        return new CategoryCollection(Category::orderBy('updated_at', 'desc')->get());
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $item = Category::create($request->all());
        return new CategoryResource($item);
    }

    public function show($id)
    {
        return new CategoryResource(Category::with('products')->find($id));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $item = Category::find($id);
        $item->update($request->all());
        return new CategoryResource($item);
    }

    public function destroy($id)
    {
        $item = Category::find($id);
        $item->delete();
        return $item;
    }
}
