<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required'
        ]);

        $file = $request->file('image')->storeAs('images', $request->file('image')->getClientOriginalName(), 'my_files');
        return response()->json($file);
    }
}
